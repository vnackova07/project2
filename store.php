<?php


require_once('db.php');


if ($_FILES['photo']['size'] > 2048000) {
    echo json_encode(["msg" => "File too large", "status" => 400]);
    die();
}

$extension = pathinfo($_FILES['photo']['name'], PATHINFO_EXTENSION);

if ($extension != 'png' && $extension != 'jpg') {
    echo json_encode(["msg" => "Unsupported file", "status" => 400]);
    die();
}


$name = generateRandomString(6);
$name .= ".$extension";

move_uploaded_file($_FILES['photo']['tmp_name'], "uploads/$name");


$image_url = "http://localhost/brainster/03.2021/demo/uploads/$name";

$apiResult = file_get_contents("https://jager.brainster.xyz/api?img=$image_url&iwantstatus=1");

$result = json_decode($apiResult, true);



$email = (isset($_POST['email']) ? $_POST['email'] : '');
$image_storage = "./uploads/$name";
$image_text = $result['text'];
$img_status = $result['img_status'];




if ($result['code'] == 200) {
    if ($result['img_status'] == 0) {
        unlink("uploads/$name");

        echo json_encode(["msg" => "Not an image, try again, you have X more tries", "status" => 500]);
        die();
    } else if ($result['img_status'] == 1 || $result['img_status'] == 2) {
        //zapisi vo baza ime naslika, email na korisnikot i status na slika
        //INSERT INTO tabela (image, status, email) VALUES ()

        $sql = "INSERT INTO user(email, image_url, image_text, status, receipt_status) VALUES('$email', '$image_storage', '$image_text', '$img_status' , 0)";
        if ($conn->query($sql) === TRUE) {
            echo "New record created successfully";
          } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
          }
          
          $conn->close();
    

        echo json_encode(["msg" => "Success", "status" => 200]);
        die();
    }
}

function generateRandomString($length = 10) {
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}



?>