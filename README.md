# Project2

1. Clone this repo with the following commands:

```
git clone git@gitlab.com:vnackova07/project2.git
cd project2/

```

2. Open localhost/phpmyadmin and make new database with name giveaway and then import file giveaway.sql .

3. Open code in VS Code and change username and password in connection.php and db.php if they're not correct.

4. Open index.html with live server or on browser and login as admin with username: admin and password admin.

