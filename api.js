document.querySelector("#upload").addEventListener("click", function () {
    
     console.log("test");
    let attempts = 0;
    

    if(localStorage.getItem("attempts") < 3){

    let email = document.querySelector("#email").value;
    let photo = document.querySelector("#photo").files[0];

    let data = new FormData();
    data.append("email", email);
    data.append("photo", photo);
    
    fetch("store.php", { method: "POST", body: data})
        .then((response) => {

            console.log("response", response);

            if (response.status == 200) {
                alert("You have successfully applied for the giveaway! ");
            } 
            else if (response.status == 500) {
                
                attempts = localStorage.getItem('attempts') ? Number(localStorage.getItem('attempts')) + 1 : 1;
                console.log(localStorage.getItem('attempts'));

                localStorage.setItem("attempts", attempts);
                alert(`Unfortunately, the picture is not a receipt, you have ${3 - attempts} attempts.`);
                
                
            } else {
                alert(response.msg);
            }

        })
    }
    else{
        alert("Sorry, you have had 3 failed attempts, please wait 24 hours for a retry.");
    }
});
