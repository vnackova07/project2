function validateImageAndSize() {
    var formData = new FormData(); 
    var file = document.getElementById("photo").files[0]; 
    formData.append("Filedata", file);
    
    var t = file.type.split('/').pop().toLowerCase(); 
    if (t != "jpeg" && t != "jpg" && t != "png") {
        document.getElementById("validation").innerHTML ="Please select a valid image file (jpg, jpeg & png) !";
        document.getElementById("photo").value = '';
        console.log("photo"); 
        return false; 
        
    } else if (t == "jpeg" || t == "jpg" || t == "png"){
        document.getElementById("validation").innerHTML =" ";
    }
    
    var fsize = (file.size / 1024 / 1024).toFixed(2); 
    if (fsize > 2 ) {
        document.getElementById("validation").innerHTML =" Max Upload size is 2MB only !"; 
        document.getElementById("photo").value = ''; 
        return false; 
    } 
    return true;
}