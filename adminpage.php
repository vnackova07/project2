<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="./img/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
    <link rel="stylesheet" href="style.css" />
    <title>Jägermeister admin</title>
</head>
<body>
    <!--Navbar-->
    <div class="container-fluid bg-white p-0">
        <nav class="navbar px-2">
            <a class="navbar-brand" href="#">
                <img src="./img/logo.png" width="70" height="70" class="d-inline-block align-top" alt="" />
                <img src="./img/logo jager.png" width="120" height="90" alt="" />
            </a>
            <form action="">
                <a href="./index.html"><button class="btn btn-warning my-2 my-sm-0" type="button">Sign Out</button></a>
            </form>
        </nav>
        <div class="container my-2 d-flex justify-content-between p-0">
    <div class="form-check">
                <input class="form-check-input card_check" type="radio" name="button" value="0" id="pending" checked />
                <label class="form-check-label" for="pending">
                    Pending
                </label>
            </div>
           
            <div class="form-check">
                <input class="form-check-input card_check" type="radio"  name="button" value="1" id="awarded" />
                <label class="form-check-label" for="awarded">
                    Awarded
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input card_check" type="radio"  name="button" value="2" id="rejected" />
                <label class="form-check-label" for="rejected">
                    Rejected
                </label>
            </div>
        </div>
            <div class="container mt-0">

            <div class="row d-flex flex-column flex-md-row justify-content-center align-items-center" id="hello">

            </div>

            </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $(document).ready(function(){
            print_cards();
            $(".card_check").click(function(){
                
                print_cards();
                
            });
          
            
                
         

                
        });

        function print_cards() {
            var action;
            
           
            action=$('input[name="button"]:checked').val();
               
               
                $.ajax({
                    url:'listCards.php',
                    method: 'POST',
                    data:{action:action},
                    success:function(response){
                    $('#hello').html(response);
                       
                       console.log("from print");
                       
                
                    
                    }
                })   
            
        }

        
        function reject(id) {
            var action = "reject";
            
           
                $.ajax({
                    url:'changeCards.php',
                    method: 'POST',
                    data:{action:action, id:id},
                    success:function(response){
                      
                      
                       print_cards();
                       console.log("rejected");
                      
                    }
                })   
           
        }

        function award(id) {
            var action = "award";
            
           
                $.ajax({
                    url:'changeCards.php',
                    method: 'POST',
                    data:{action:action, id:id},
                    success:function(response){
                      
                      
                       print_cards();
                       console.log("awarded");
                    }
                })   
           
        }


        
            
     

        

    </script>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
</body>
</html>